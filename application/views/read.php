<?php $this->load->view('layout/header'); ?>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
	<div id="wrapper">

		<nav class="navbar navbar-custom navbar-fixed-top" id="nav-x" role="navigation">
			<div class="top-area">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<p class="bold text-left">Senin - Sabtu, 08.00 - 20.00 </p>
						</div>
						<div class="col-sm-6 col-md-6">
							<p class="bold text-right">Telpon kami sekarang +6221 - 880 8081 / 8999 4101</p>
						</div>
					</div>
				</div>
			</div>
			<div class="container navigation">

				<div class="navbar-header page-scroll">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
						<i class="fa fa-bars"></i>
					</button>
					<a class="navbar-brand">
						<img style="float:left;" src="<?php echo base_url() ?>assets/img/logo1.png" alt="" width="40" height="40" />
						<h2 class="h-ultra" id="h-ultra-1">Artikel Kesehatan</h2>
					</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right navbar-main-collapse">
					<ul class="nav navbar-nav">
						<li><a href="<?php echo base_url() ?>">Profil Heddi Medica</a></li>
						<li><a href="<?php echo base_url() ?>formulir">Booking Online</a></li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container -->
		</nav>
		<section id="intro" class="intro paddingbot-60" style="background: #fff;">
			<div class="intro-content" style="background: #fff;">
				<div class="container marginbot-50">
					<div class="row">
						<div class="col-lg-8 col-lg-offset-2" id="title-x">
							<div class="wow lightSpeedIn" data-wow-delay="0.1s">
								<div class="section-heading">
									<h3 class="h-bold">
										<?php echo $post[0]['title']?>
									</h3>
								</div>
								Di Publikasikan Tanggal :
								<?php echo to_date_time($post[0]['publish_at']) ?> Oleh
								<?php echo $this->main_model->gdo4p('users', 'name', 'e_id', $post[0]['e_id']) ?>
								<hr>
								<div class="fill">
									<?php echo $post[0]['fill'] ?>
								</div>
								<p style="font-weight: bold; font-style: italic; color: #3fbbc0;">Apakah Artikel <a href="<?php echo base_url() ?>artikel/read/<?php echo $post[0]['slug'] ?>"
									 style="cursor: pointer; color: #3fbbc0; text-decoration: underline;">
										<?php echo $post[0]['title'] ?></a> cukup membantu ? jika masih
									penasaran YUK..! Segera kunjungi dokter di Klinik Heddi Medica untuk konsultasi dan lainnya !. Silakan isi
									data
									diri kamu
									untuk Booking Online dokter pilihan kamu :)</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="form" id="form-x" style="display: flex; align-items: center; justify-content: center;">
				<div class="col-lg-7 col-sm-7 col-md-7">
					<div class="wow lightSpeedIn" data-wow-delay="0.1s">
						<div class="panel panel-skin">
							<div class="panel-heading">
								<h3 class="panel-title"><span class="fa fa-pencil-square-o"></span> Buat Janji Temu Dokter <small>(Gratis
										!!)</small></h3>
							</div>
							<div class="panel-body">
								<form class="contactForm lead">
									<div class="row">
										<div class="col-xs-6 col-sm-6 col-md-6">
											<div class="form-group">
												<label>Nama</label>
												<input onkeyup="check_form()" type="text" name="first_name" id="name" class="form-control input-md">
											</div>
										</div>
										<div class="col-xs-6 col-sm-6 col-md-6">
											<div class="form-group">
												<label>Poli Klinik</label>
												<select onchange="check_form()" name="specialist" id="specialist" class="form-control input-md">
													<option value="">- Pilih Poli Klinik -</option>
													<?php foreach ($specialists as $key => $value) { ?>
													<option value="<?php echo $value['s_code']?>">
														<?php echo $value['name'] ?>
													</option>
													<?php } ?>
												</select>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-6 col-sm-6 col-md-6">
											<div class="form-group">
												<label>Email</label>
												<input onkeyup="check_format()" type="email" name="email" id="email" class="form-control input-md">
												<p style="color: red; display: none; font-size: 18px;" id="invalid_email">Format email tidak benar !</p>
											</div>
										</div>
										<div class="col-xs-6 col-sm-6 col-md-6">
											<div class="form-group">
												<label>Nomor Telpon</label>
												<input onkeyup="check_form()" type="text" name="phone" id="phone" class="form-control input-md">
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12">
											<div class="form-group">
												<label>Pesan</label>
												<textarea onkeyup="check_form()" name="message" id="message" class="form-control input-md"></textarea>
											</div>
										</div>
									</div>

									<a onclick="send()" id="send_message" disabled class="btn btn-skin btn-block btn-lg"> Kirim Permintaan</a>
									<a id="loading_message" style="display: none;" disabled class="btn btn-skin btn-block btn-lg"> Mohon Tunggu
										..</a>
									<p class="lead-footer">* Kami akan segera menghubungi anda via emai atau telpon</p>

								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

	</div>
	</section>

	<!-- <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <ins class="adsbygoogle" style="display:block; text-align:center;" data-ad-layout="in-article" data-ad-format="fluid"
        data-ad-client="ca-pub-4602769966928907" data-ad-slot="2800884355"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script> -->

	<footer>

		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-6">
					<div class="wow fadeInDown" data-wow-delay="0.1s">
						<div class="widget">
						<h5>Artikel Terbaru</h5>
							<div class="row">
								<ul class="lead-list">
									<?php foreach ($recent as $key => $value) { ?>
									<li>
										<table>
											<tr>
												<td><a href="<?php echo base_url() ?>artikel/read/<?php echo $value['slug'] ?>"><img src="http://localhost/ci/hmedica/assets/admin/posts/<?php echo $value['image_title'] ?>"
														 width="80px;" hegiht="80px;"></td>
												<td>
													<strong>
														<a href="<?php echo base_url() ?>artikel/read/<?php echo $value['slug'] ?>">
															<?php echo $value['title'] ?></a>
													</strong>
													<br />
													<?php limiter($value['fill'], 90) ?>
												</td>
											</tr>
										</table>
									</li>
									<?php } ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-6">
					<div class="wow fadeInDown" data-wow-delay="0.1s">
						<div class="widget">
							<h5>Informasi Kontak</h5>
							<p>
								Kav. Karang Jaya RT.01 RW.126 Karang Satri Bekasi
								(+/- 200 Meter dari SMK Karya Guna Bhakti 1 Samping Masjid Jami AT-Taubah)
							</p>
							<ul>
								<li>
									<span class="fa-stack fa-lg">
										<i class="fa fa-circle fa-stack-2x"></i>
										<i class="fa fa-calendar-o fa-stack-1x fa-inverse"></i>
									</span> Senin - Minggu, 08.00 - 20.00
								</li>
								<li>
									<span class="fa-stack fa-lg">
										<i class="fa fa-circle fa-stack-2x"></i>
										<i class="fa fa-phone fa-stack-1x fa-inverse"></i>
									</span> +6221 - 880 8081 / 8999 4101
								</li>
								<li style="color: limegreen;">
									<span class="fa-stack fa-lg">
										<i class="fa fa-circle fa-stack-2x"></i>
										<i class="fa fa-phone fa-stack-1x fa-inverse"></i>
									</span>
									<a style="text-decoration: underline; color: blue;" href="whatsapp://send?text=Hallo&phone=+628983797615&abid=+628983797615">Click
										to WA : +628983797615</a>
								</li>
								<li>
									<span class="fa-stack fa-lg">
										<i class="fa fa-circle fa-stack-2x"></i>
										<i class="fa fa-envelope-o fa-stack-1x fa-inverse"></i>
									</span> klinikheddimedica@gmail.com
								</li>

							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-6"></div>

				<div class="col-sm-6 col-md-3">
					<div class="wow fadeInDown" data-wow-delay="0.1s">
						<div class="widget">
							<h5>Ikuti Kami</h5>
							<ul class="company-social">
								<li class="social-facebook"><a target="_blank" href="https://www.facebook.com/heddi.medica.1"><i class="fa fa-facebook"></i></a></li>
								<li class="social-twitter" id="instagram"><a target="_blank" href="https://www.instagram.com/heddi_medica_clinic"><i
										 class="fa fa-instagram"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="sub-footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6 col-lg-6">
						<div class="wow fadeInLeft" data-wow-delay="0.1s">
							<div class="text-left">
								<p>&copy;Copyright - Heddi Medica Healt Group.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<style>
		.company-social #instagram a i {
			background: purple;
		}

		td {
			padding: 5px;
		}

		@media only screen and (min-width: 600px) {
			.fill img {
				width: 100%;
				height: auto;
				margin-bottom: 10px;
				margin-top: 10px;
			}

			#h-ultra-1 {
				width: 200%;
				margin-top: 0.8%;
			}

			#form-x {
				margin-top: -8%;
			}
		}

		@media only screen and (max-width: 599px) {
			.fill img {
				width: 100%;
				height: auto;
				margin-bottom: 10px;
				margin-top: 10px;
			}

			.fill p {
				margin-bottom: 0px;
			}

			#h-ultra-1 {
				width: 200%;
				margin-top: 3.8%;
				font-size: 25px;
			}

			#form-x {
				margin-top: -30%;
			}
		}
	</style>

	</div>
	<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
	<!-- Core JavaScript Files -->
	<script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.easing.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/wow.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.scrollTo.js"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.appear.js"></script>
	<script src="<?php echo base_url() ?>assets/js/stellar.js"></script>
	<script src="<?php echo base_url() ?>assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/nivo-lightbox.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/custom.js"></script>
	<script src="<?php echo base_url() ?>assets/js/main.js"></script>
	<script src="<?php echo base_url() ?>assets/sweetalert/sweetalert.min.js"></script>

	<script id="readjs">

		function check_form() {
			var name = $("#name").val();
			var specialist = $("#specialist").val();
			var phone = $("#phone").val();
			if (name != '' && specialist != '' && phone != '') {
				if (this.email_check == 'true') {
					$("#send_message").removeAttr('disabled');
				} else {
					$("#send_message").attr('disabled', 'disabled');
				}
			} else {
				$("#send_message").attr('disabled', 'disabled');
			}

		}

		function reset() {
			$("#name").val('');
			$("#specialist").val('');
			$("#email").val('');
			$("#phone").val('');
			$("#message").val('');
			this.email_check = '';
		}

		function check_format() {
			var email = $("#email").val();
			if (email != '') {
				if (ValidateEmail(email, "#send_message", "#invalid_email")) {
					this.email_check = 'true';
					setTimeout(() => {
						check_form();
					}, 100);
				}
			} else {
				$("#invalid_email").hide();
				$("#send_message").attr('disabled', 'disabled');
			}
		}

		function send() {
			$("#send_message").hide();
			$("#loading_message").show();
			var data = {
				name: $("#name").val(),
				specialist: $('#specialist').val(),
				email: $('#email').val(),
				phone: $('#phone').val(),
				message: $('#message').val()
			}

			var url = '<?php echo base_url() ?>web_controller/store';
			var message = 'Berhasil mengirim pesan, silakan tunggu admin kami menghubungi anda.';
			var error = 'Gagal mengirim pesan.';

			postData(url, data, function (err, response) {
				if (response) {
					var status = response.status;
					if (status == 'success') {
						$("#send_message").show();
						$("#loading_message").hide();
						swal("Sukses", message, "success");
						reset();
					} else {
						$("#send_message").show();
						$("#loading_message").hide();
						swal("Terjadi Kesalahan", error, "error");
					}
				} else {
					console.log('ini error : ', err);
				}
			});
		}

		document.getElementById('readjs').innerHTML = "";
	</script>
</body>

</html>