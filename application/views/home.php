<?php
$this->load->view('layout/header');
?>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
	<div id="wrapper">

		<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
			<div class="top-area">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<p class="bold text-left">Senin - Sabtu, 08.00 - 20.00 </p>
						</div>
						<div class="col-sm-6 col-md-6">
							<p class="bold text-right">Telpon kami sekarang +6221 - 880 8081 / 8999 4101</p>
						</div>
					</div>
				</div>
			</div>
			<div class="container navigation">

				<div class="navbar-header page-scroll">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
						<i class="fa fa-bars"></i>
					</button>
					<a class="navbar-brand">
						<img style="float:left;" src="<?php echo base_url() ?>assets/img/logo1.png" alt="" width="40" height="40" />
						<h2 class="h-ultra" id="h-ultra-1">Heddi Medica</h2>
					</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right navbar-main-collapse">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#intro">Profil</a></li>
						<li><a href="#service">Pelayanan</a></li>
						<li><a href="#doctor">Dokter</a></li>
						<li><a href="#facilities">Fasilitas</a></li>
						<li><a href="#promo">Promo</a></li>
						<li><a href="#pricing">Jadwal Praktek</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="badge custom-badge red pull-right">Extra</span>More
								<b class="caret"></b></a>
							<ul class="dropdown-menu">
							<li><a href="<?php echo base_url() ?>artikel/1">Artikel Kesehatan</a></li>
							<li><a href="<?php echo base_url() ?>formulir">Booking Online</a></li>
							</ul>
						</li>
						
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container -->
		</nav>
		<!-- Section: intro -->
		<section id="intro" class="intro">
			<div class="intro-content">
				<div class="container">
					<div class="row">
						<div class="col-lg-6">
							<div class="wow fadeInDown" data-wow-offset="0" data-wow-delay="0.1s">
								<h2 class="h-ultra">Heddi Medica Group</h2>
							</div>
							<div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.1s">
								<h4 class="h-light">Menyediakan pelayanan kesehatan terbaik untuk anda</h4>
							</div>
							<div class="well well-trans">
								<div class="wow fadeInRight" data-wow-delay="0.1s">

									<ul class="lead-list">
										<li><span class="fa fa-check fa-2x icon-success"></span> <span class="list"><strong>Dokter Umum</strong><br />Melayanai
												dengan hati untuk masyarakat menengah kebawah</span></li>
										<li><span class="fa fa-check fa-2x icon-success"></span> <span class="list"><strong>Dokter Gigi</strong><br />Pelayanan
												dengan teknologi terbaru dengan harga terjangkau</span></li>
										<li><span class="fa fa-check fa-2x icon-success"></span> <span class="list"><strong>Kebidanan</strong><br />Tenaga
												medis berpengalaman lebih dari 10 tahun</span></li>
									</ul>
									<!-- <p class="text-right wow bounceIn" data-wow-delay="0.4s">
                  <a href="#" class="btn btn-skin btn-lg">Learn more <i class="fa fa-angle-right"></i></a>
                </p> -->
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
								<img src="<?php echo base_url() ?>assets/img/dummy/img-1.png" class="img-responsive" alt="" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<style>
			@media only screen and (min-width: 600px) {
				#h-ultra-1 {
					width: 200%;
					margin-top: 0.8%;
				}
			}

			@media only screen and (max-width: 599px) {
				#h-ultra-1 {
					width: 200%;
					margin-top: 5.8%;
					font-size: 25px;
				}
			}
		</style>

		<!-- /Section: intro -->
		<?php
$this->load->view('layout/lower_content');
$this->load->view('layout/footer');
?>