<?php $this->load->view('layout/header'); ?>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
	<div id="wrapper">

		<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
			<div class="top-area">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<p class="bold text-left">Senin - Sabtu, 08.00 - 20.00 </p>
						</div>
						<div class="col-sm-6 col-md-6">
							<p class="bold text-right">Telpon kami sekarang +6221 - 880 8081 / 8999 4101</p>
						</div>
					</div>
				</div>
			</div>
			<div class="container navigation">

				<div class="navbar-header page-scroll">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
						<i class="fa fa-bars"></i>
					</button>
					<a class="navbar-brand">
						<img style="float:left;" src="<?php echo base_url() ?>assets/img/logo1.png" alt="" width="40" height="40" />
						<h2 class="h-ultra" id="h-ultra-1">Booking Online</h2>
					</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right navbar-main-collapse">
					<ul class="nav navbar-nav">
						<li><a href="<?php echo base_url() ?>">Profil Heddi Medica</a></li>
						<li><a href="<?php echo base_url() ?>artikel/1">Artikel Kesehatan</a></li>

					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container -->
		</nav>

		<!-- Section: intro -->
		<section id="intro" class="intro">
			<div class="intro-content">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-wrapper">
								<div class="wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.2s">

									<div class="panel panel-skin">
										<div class="panel-heading">
											<h3 class="panel-title"><span class="fa fa-pencil-square-o"></span> Buat Janji Temu Dokter <small>(Gratis
													!!)</small></h3>
										</div>
										<div class="panel-body">
											<form class="contactForm lead">
												<div class="row">
													<div class="col-xs-6 col-sm-6 col-md-6">
														<div class="form-group">
															<label>Nama</label>
															<input onkeyup="check_form()" type="text" name="first_name" id="name" class="form-control input-md">
														</div>
													</div>
													<div class="col-xs-6 col-sm-6 col-md-6">
														<div class="form-group">
															<label>Poli Klinik</label>
															<select onchange="check_form()" name="specialist" id="specialist" class="form-control input-md">
																<option value="">- Pilih Poli Klinik -</option>
																<?php foreach ($specialists as $key => $value) { ?>
																<option value="<?php echo $value['s_code']?>">
																	<?php echo $value['name'] ?>
																</option>
																<?php } ?>
															</select>
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-xs-6 col-sm-6 col-md-6">
														<div class="form-group">
															<label>Email</label>
															<input onkeyup="check_format()" type="email" name="email" id="email" class="form-control input-md">
															<p style="color: red; display: none; font-size: 18px;" id="invalid_email">Format email tidak benar !</p>
														</div>
													</div>
													<div class="col-xs-6 col-sm-6 col-md-6">
														<div class="form-group">
															<label>Nomor Telpon</label>
															<input onkeyup="check_form()" type="text" name="phone" id="phone" class="form-control input-md">
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-12">
														<div class="form-group">
															<label>Pesan</label>
															<textarea onkeyup="check_form()" name="message" id="message" class="form-control input-md"></textarea>
														</div>
													</div>
												</div>

												<a onclick="send()" id="send_message" disabled class="btn btn-skin btn-block btn-lg"> Kirim Permintaan</a>
												<a id="loading_message" style="display: none;" disabled class="btn btn-skin btn-block btn-lg"> Mohon Tunggu
													..</a>
												<p class="lead-footer">* Kami akan segera menghubungi anda via emai atau telpon</p>

											</form>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- /Section: intro -->

		<style>
			@media only screen and (min-width: 600px) {
				#h-ultra-1 {
					width: 200%;
					margin-top: 0.8%;
				}
			}

			@media only screen and (max-width: 599px) {
				#h-ultra-1 {
					width: 200%;
					margin-top: 4.8%;
					font-size: 25px;
				}
			}
		</style>

		<script id="formjs">
			function check_form() {
				var name = $("#name").val();
				var specialist = $("#specialist").val();
				var phone = $("#phone").val();
				if (name != '' && specialist != '' && phone != '') {
					if (this.email_check == 'true') {
						$("#send_message").removeAttr('disabled');
					} else {
						$("#send_message").attr('disabled', 'disabled');
					}
				} else {
					$("#send_message").attr('disabled', 'disabled');
				}

			}

			function reset() {
				$("#name").val('');
				$("#specialist").val('');
				$("#email").val('');
				$("#phone").val('');
				$("#message").val('');
				this.email_check = '';
			}

			function check_format() {
				var email = $("#email").val();
				if (email != '') {
					if (ValidateEmail(email, "#send_message", "#invalid_email")) {
						this.email_check = 'true';
						setTimeout(() => {
							check_form();
						}, 100);
					}
				} else {
					$("#invalid_email").hide();
					$("#send_message").attr('disabled', 'disabled');
				}
			}

			function send() {
				$("#send_message").hide();
				$("#loading_message").show();
				var data = {
					name: $("#name").val(),
					specialist: $('#specialist').val(),
					email: $('#email').val(),
					phone: $('#phone').val(),
					message: $('#message').val()
				}

				var url = 'web_controller/store';
				var message = 'Berhasil mengirim pesan, silakan tunggu admin kami menghubungi anda.';
				var error = 'Gagal mengirim pesan.';

				postData(url, data, function (err, response) {
					if (response) {
						var status = response.status;
						if (status == 'success') {
							$("#send_message").show();
							$("#loading_message").hide();
							swal("Sukses", message, "success");
							reset();
						} else {
							$("#send_message").show();
							$("#loading_message").hide();
							swal("Terjadi Kesalahan", error, "error");
						}
					} else {
						console.log('ini error : ', err);
					}
				});
			}

			document.getElementById('formjs').innerHTML = "";
		</script>
		<?php $this->load->view('layout/footer'); ?>