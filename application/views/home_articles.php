<?php $this->load->view('layout/header'); ?>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
	<div id="wrapper">

		<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
			<div class="top-area">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<p class="bold text-left">Senin - Sabtu, 08.00 - 20.00 </p>
						</div>
						<div class="col-sm-6 col-md-6">
							<p class="bold text-right">Telpon kami sekarang +6221 - 880 8081 / 8999 4101</p>
						</div>
					</div>
				</div>
			</div>
			<div class="container navigation">

				<div class="navbar-header page-scroll">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
						<i class="fa fa-bars"></i>
					</button>
					<a class="navbar-brand">
						<img style="float:left;" src="<?php echo base_url() ?>assets/img/logo1.png" alt="" width="40" height="40" />
						<h2 class="h-ultra" id="h-ultra-1">Artikel Kesehatan</h2>
					</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right navbar-main-collapse">
					<ul class="nav navbar-nav">
						<li><a href="<?php echo base_url() ?>">Profil Heddi Medica</a></li>
						<li><a href="<?php echo base_url() ?>formulir">Booking Online</a></li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container -->
		</nav>

		<section id="intro" class="intro" style="background: #fff;">
			<div class="intro-content" style="background: #fff;">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
						<div class="section-heading text-center">
								<p>Perbanyak pengetahuan kesehatan anda dan siagakan kesehatan anda bersama Heddi Medica melayani dengan 
								prima & profesional. </p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="prevnext" style="display: flex; align-items: center; justify-content: center;">
				<table>
					<tr>
						<?php if($page > 1){ ?>
						<td><a href="<?php echo base_url() ?>artikel/<?php echo $page-1;?>" class="btn btn-skin btn-block btn-lg">
								Prev</a></td>
						<?php }else{ ?>
						<td><a disabled class="btn btn-skin btn-block btn-lg"> Prev</a></td>
						<?php } ?>
						<td style="font-weight: bold;">
							<?php echo $page ?> Of
							<?php echo $all ?>
						</td>
						<td><a href="<?php echo base_url() ?>artikel/<?php echo $page+1;?>" class="btn btn-skin btn-block btn-lg">
								Next</a></td>
					</tr>
				</table>
			</div>

			<div class="container">
				<div class="row">
					<?php if(!empty($posts)){ foreach ($posts as $key => $value) {?>
					<div style="padding: 10px;" class="col-sm-4 col-md-4">
						<div class="partner">
							<a href="<?php echo base_url() ?>artikel/read/<?php echo $value['slug'] ?>"><img style="width: 100%; max-height: 175px; min-width: 300px;"
								 src="http://localhost/ci/hmedica/assets/admin/posts/<?php echo $value['image_title'] ?>" /></a>
							<div id="square" style="width: 100%; height: 25px; border: 1px solid #ccc; background: #3fbbc0;">
								<p style="color: #fff; font-size: 12px;">Publihser :
									<?php echo $this->main_model->gdo4p('users','name','e_id',$value['e_id']) ?>
								</p>
							</div>
						</div>
						<a href="<?php echo base_url() ?>artikel/read/<?php echo $value['slug'] ?>" class="cbp-singlePage cbp-l-grid-team-name"
						 style="text-align: left; font-size: 14px; margin-top: 10px; text-decoration: none;cursor: pointer;">
							<?php echo $value['title'] ?>
						</a>
						<p>
							<?php echo limiter($value['fill'], 120) ?>
						</p>
					</div>
					<?php } }else{?>
					<div class="col-sm-4 col-md-4"></div>
					<div class="col-sm-4 col-md-4">
						<div class="partner">
							<img style="width: 100%; max-height: 175px; min-width: 300px;" src="<?php echo base_url() ?>assets/img/stop.png" />
							<div id="square" style="width: 100%; height: 25px;">
								<p style="color: #000; font-size: 12px;">Ooops..! Tidak ada artikel :)

								</p>
							</div>
						</div>
					</div>
					<div class="col-sm-4 col-md-4"></div>
					<?php } ?>
				</div>
			</div>

			<div class="prevnext" style="display: flex; align-items: center; justify-content: center;">
				<table>
					<tr>
						<?php if($page > 1){ ?>
						<td><a href="<?php echo base_url() ?>artikel/<?php echo $page-1;?>" class="btn btn-skin btn-block btn-lg">
								Prev</a></td>
						<?php }else{ ?>
						<td><a disabled class="btn btn-skin btn-block btn-lg"> Prev</a></td>
						<?php } ?>
						<td style="font-weight: bold;">
							<?php echo $page ?> Of
							<?php echo $all ?>
						</td>
						<td><a href="<?php echo base_url() ?>artikel/<?php echo $page+1;?>" class="btn btn-skin btn-block btn-lg">
								Next</a></td>
					</tr>
				</table>
			</div>
		</section>

		<style>
			td {
				padding: 5px;
			}

			@media only screen and (min-width: 600px) {
				#h-ultra-1 {
					width: 200%;
					margin-top: 0.8%;
				}
			}

			@media only screen and (max-width: 599px) {
				#h-ultra-1 {
					width: 200%;
					margin-top: 3.8%;
					font-size: 25px;
				}
			}
		</style>

		<?php $this->load->view('layout/footer'); ?>