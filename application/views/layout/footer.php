<!-- <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" style="display:block; text-align:center;" data-ad-layout="in-article" data-ad-format="fluid"
 data-ad-client="ca-pub-4602769966928907" data-ad-slot="2800884355"></ins>
<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
</script> -->

<footer>

	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-4">
				<div class="wow fadeInDown" data-wow-delay="0.1s">
					<div class="widget">
						<h5>Tentang Heddi Medica</h5>
						<p>
							Kami adalah klinik pratama yang Prima dan Profesional yang
							memiliki misi meningkatkan derajat kesehatan masyarakat yang prima
							dengan biaya yang terjangkau.
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="wow fadeInDown" data-wow-delay="0.1s">
					<div class="widget">
						<h5>Informasi Kontak</h5>
						<p>
							Kav. Karang Jaya RT.01 RW.126 Karang Satri Bekasi
							(+/- 200 Meter dari SMK Karya Guna Bhakti 1 Samping Masjid Jami AT-Taubah)
						</p>
						<ul>
							<li>
								<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-calendar-o fa-stack-1x fa-inverse"></i>
								</span> Senin - Minggu, 08.00 - 20.00
							</li>
							<li>
								<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-phone fa-stack-1x fa-inverse"></i>
								</span> +6221 - 880 8081 / 8999 4101
							</li>
							<li style="color: limegreen;">
								<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-phone fa-stack-1x fa-inverse"></i>
								</span>
								<a style="text-decoration: underline; color: blue;" href="whatsapp://send?text=Hallo&phone=+628983797615&abid=+628983797615">Click
									to WA : +628983797615</a>
							</li>
							<li>
								<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-envelope-o fa-stack-1x fa-inverse"></i>
								</span> klinikheddimedica@gmail.com
							</li>

						</ul>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="wow fadeInDown" data-wow-delay="0.1s">
					<div class="widget">
						<h5>Ikuti Kami</h5>
						<ul class="company-social">
							<li class="social-facebook"><a target="_blank" href="https://www.facebook.com/heddi.medica.1"><i class="fa fa-facebook"></i></a></li>
							<li class="social-twitter" id="instagram"><a target="_blank" href="https://www.instagram.com/heddi_medica_clinic"><i
									 class="fa fa-instagram"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-6 col-lg-6">
					<div class="wow fadeInLeft" data-wow-delay="0.1s">
						<div class="text-left">
							<p>&copy;Copyright - Heddi Medica Healt Group.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>


<style>
	.company-social #instagram a i {
		background: purple;
	}
</style>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- Core JavaScript Files -->
<script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/wow.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.scrollTo.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.appear.js"></script>
<script src="<?php echo base_url() ?>assets/js/stellar.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/nivo-lightbox.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/custom.js"></script>
<script src="<?php echo base_url() ?>assets/js/main.js"></script>
<script src="<?php echo base_url() ?>assets/sweetalert/sweetalert.min.js"></script>

</body>

</html>