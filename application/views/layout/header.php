<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="theme-color" content="#3fbbc0" />


	<title>Heddi Medica</title>

	<!-- css -->
	<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url() ?>assets/plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/nivo-lightbox.css" rel="stylesheet" />
	<link href="<?php echo base_url() ?>assets/css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url() ?>assets/css/owl.carousel.css" rel="stylesheet" media="screen" />
	<link href="<?php echo base_url() ?>assets/css/owl.theme.css" rel="stylesheet" media="screen" />
	<link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet" />
	<link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/sweetalert/sweetalert.css" rel="stylesheet" />

	<link rel="icon" href="<?php echo base_url() ?>assets/img/heddi.png" type="image/x-icon" />

	<!-- boxed bg -->
	<link id="bodybg" href="<?php echo base_url() ?>assets/bodybg/bg1.css" rel="stylesheet" type="text/css" />
	<!-- template skin -->
	<link id="t-colors" href="<?php echo base_url() ?>assets/color/default.css" rel="stylesheet">
	
	<!-- <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
         (adsbygoogle = window.adsbygoogle || []).push({
              google_ad_client: "ca-pub-4602769966928907",
              enable_page_level_ads: true
         });
    </script> -->

	<!-- =======================================================
    Theme Name: Medicio
    Theme URL: https://bootstrapmade.com/medicio-free-bootstrap-theme/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>