<!-- Section: boxes -->
<section id="boxes" class="home-section paddingtop-80">

	<div class="container">
		<div class="row">
			<div class="col-sm-3 col-md-3">
				<div class="wow fadeInUp" data-wow-delay="0.2s">
					<div class="box text-center">

						<i class="fa fa-check fa-3x circled bg-skin"></i>
						<h4 class="h-bold">Buat Janji Temu Dokter</h4>
						<p>
							Ingin melakukan pemeriksaan Umum, Gigi dan Kebidanan tanpa antri ?
							buat janji temu doktermu sendiri secara onlime melalui heddimedica.com
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-3 col-md-3">
				<div class="wow fadeInUp" data-wow-delay="0.2s">
					<div class="box text-center">

						<i class="fa fa-list-alt fa-3x circled bg-skin"></i>
						<h4 class="h-bold">Biaya Pemeriksaan</h4>
						<p>
							Dengan teknologi terbaru kami dibidang Poli Gigi,
							kami tetap mengedepankan pelayanan terhadap masyarakat
							menengah kebawah dengan tarif yang sangat terjangkau
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-3 col-md-3">
				<div class="wow fadeInUp" data-wow-delay="0.2s">
					<div class="box text-center">
						<i class="fa fa-user-md fa-3x circled bg-skin"></i>
						<h4 class="h-bold">Tenaga Ahli</h4>
						<p>
							Kami didukung oleh tenaga ahli di bidang Polig Gigi dan Kebidanan yang sangat berpengalaman dibidangnya
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-3 col-md-3">
				<div class="wow fadeInUp" data-wow-delay="0.2s">
					<div class="box text-center">

						<i class="fa fa-hospital-o fa-3x circled bg-skin"></i>
						<h4 class="h-bold">Catatan Medis</h4>
						<p>
							Rekam medismu tersimpan aman di server, kami jaga kerahasiaan rekam medis anda dengan keamanan yang mumpuni yang
							telah tertanam di server kami
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>
<!-- /Section: boxes -->


<section id="callaction" class="home-section paddingtop-40">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="callaction bg-gray">
					<div class="row">
						<div class="col-md-8">
							<div class="wow fadeInUp" data-wow-delay="0.1s">
								<div class="cta-text">
									<h3>Apa saja layanan kami ?</h3>
									<p></p>
								</div>
							</div>
						</div>
						<!-- <div class="col-md-4">
							<div class="wow lightSpeedIn" data-wow-delay="0.1s">
								<div class="cta-btn">
									<a href="#" class="btn btn-skin btn-lg">Book an appoinment</a>
								</div>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<!-- Section: services -->
<section id="service" class="home-section nopadding paddingtop-60">

	<div class="container">

		<div class="row">
			<div class="col-sm-6 col-md-6">
				<div class="wow fadeInUp" data-wow-delay="0.2s">
					<img src="<?php echo base_url() ?>assets/img/dummy/img-1.jpg" class="img-responsive" alt="" />
				</div>
			</div>
			<div class="col-sm-3 col-md-3">

				<div class="wow fadeInRight" data-wow-delay="0.1s">
					<div class="service-box">
						<div class="service-icon">
							<span class="fa fa-stethoscope fa-3x"></span>
						</div>
						<div class="service-desc">
							<h5 class="h-light">Rawat Jalan</h5>
							<p>Rawat jalan dengan dokter - dokter profesional yang melayani dengan prima dan profesional.</p>
						</div>
					</div>
				</div>

				<div class="wow fadeInRight" data-wow-delay="0.2s">
					<div class="service-box">
						<div class="service-icon">
							<span class="fa fa-wheelchair fa-3x"></span>
						</div>
						<div class="service-desc">
							<h5 class="h-light">Alat Medis & Kesehatan</h5>
							<p>Heddi Medica dilengkapi dengan alat medis yang lengkap dengan teknologi terbaru yang siap
								digunakan untuk memberikan pelayanan terbaik untuk anda.</p>
						</div>
					</div>
				</div>
				<div class="wow fadeInRight" data-wow-delay="0.3s">
					<div class="service-box">
						<div class="service-icon">
							<span class="fa fa-plus-square fa-3x"></span>
						</div>
						<div class="service-desc">
							<h5 class="h-light">Apotek</h5>
							<p>Kami menyediakan obat generik & paten terbaik dengan harga terjangkau dengan tenaga farmasi yang profesional.</p>
						</div>
					</div>
				</div>


			</div>
			<div class="col-sm-3 col-md-3">

				<div class="wow fadeInRight" data-wow-delay="0.1s">
					<div class="service-box">
						<div class="service-icon">
							<span class="fa fa-h-square fa-3x"></span>
						</div>
						<div class="service-desc">
							<h5 class="h-light">Rawat Inap</h5>
							<p>Tersedia kamar yang nyaman untuk pasien rawat inap, dengan kapasitas maksimal lebih dari 2 orang pasien.</p>
						</div>
					</div>
				</div>

				<div class="wow fadeInRight" data-wow-delay="0.2s">
					<div class="service-box">
						<div class="service-icon">
							<span class="fa fa-filter fa-3x"></span>
						</div>
						<div class="service-desc">
							<h5 class="h-light">Pengecekan Laboratorium</h5>
							<p>Kami melayani pengecekan gula darah, kolestrol, imunisasi, pemeriksaan kehamilan dan lain-lain.</p>
						</div>
					</div>
				</div>
				<div class="wow fadeInRight" data-wow-delay="0.3s">
					<div class="service-box">
						<div class="service-icon">
							<span class="fa fa-user-md fa-3x"></span>
						</div>
						<div class="service-desc">
							<h5 class="h-light"> Dokter Spesialis</h5>
							<p>Setiap poli Umum, Gigi dan Kebidanan ditangani oleh dokter sesuai bidangnya yang siap melayani anda dengan
								pelayanan terbaik secara prima dan profesional.</p>
						</div>
					</div>
				</div>

			</div>

		</div>
	</div>
</section>
<!-- /Section: services -->


<!-- Section: team -->
<section id="doctor" class="home-section bg-gray paddingbot-60">
	<div class="container marginbot-50">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<div class="wow fadeInDown" data-wow-delay="0.1s">
					<div class="section-heading text-center">
						<h2 class="h-bold"> Dokter & Bidan</h2>
						<p>Muda & Profesional, merupakan gambaran tenaga medis Heddi Medica yang merupakan aset berharga kami</p>
					</div>
				</div>
				<div class="divider-short"></div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-lg-12">

				<div id="filters-container" class="cbp-l-filters-alignCenter">
					<div data-filter="*" class="cbp-filter-item-active cbp-filter-item">Semua 
						<div class="cbp-filter-counter"></div></div>
					<div data-filter=".umum" class="cbp-filter-item">Dokter Umum 
						<div class="cbp-filter-counter"></div></div>
					<div data-filter=".gigi" class="cbp-filter-item">Dokter Gigi 
						<div class="cbp-filter-counter"></div></div>
					<div data-filter=".kebidanan" class="cbp-filter-item">Kandungan 
						<div class="cbp-filter-counter"></div></div>
				</div>

				<div id="grid-container" class="cbp-l-grid-team">
					<ul>
						<li class="cbp-item umum">
							<a class="cbp-caption cbp-singlePage">
								<div class="cbp-caption-defaultWrap">
									<img src="<?php echo base_url() ?>assets/img/team/fitri.jpg" alt="" width="100%">
								</div>
								<div class="cbp-caption-activeWrap">
									<div class="cbp-l-caption-alignCenter">
										<div class="cbp-l-caption-body">
											<div class="cbp-l-caption-text">DOKTER UMUM</div>
										</div>
									</div>
								</div>
							</a>
							<a class="cbp-singlePage cbp-l-grid-team-name" style="font-size: 14px; text-decoration: none;cursor: pointer;">Dr. Siti</a>
							<div class="cbp-l-grid-team-position" style="font-size: 12px;">SIP: No.503/470/Dinkes/DU/2018</div>
						</li>
						<li class="cbp-item gigi">
							<a class="cbp-caption cbp-singlePage">
								<div class="cbp-caption-defaultWrap">
									<img src="<?php echo base_url() ?>assets/img/team/winda.jpg" alt="" width="100%">
								</div>
								<div class="cbp-caption-activeWrap">
									<div class="cbp-l-caption-alignCenter">
										<div class="cbp-l-caption-body">
											<div class="cbp-l-caption-text">POLI GIGI</div>
										</div>
									</div>
								</div>
							</a>
							<a class="cbp-singlePage cbp-l-grid-team-name" style="font-size: 14px; text-decoration: none;cursor: pointer;">Dr. Winda Mawaddah</a>
							<div class="cbp-l-grid-team-position" style="font-size: 12px;">SIP: No.503/143/Dinkes/Drg/2018</div>
						</li>
						<li class="cbp-item kebidanan">
							<a class="cbp-caption cbp-singlePage">
								<div class="cbp-caption-defaultWrap">
									<img src="<?php echo base_url() ?>assets/img/team/heddi.jpg" alt="" width="100%">
								</div>
								<div class="cbp-caption-activeWrap">
									<div class="cbp-l-caption-alignCenter">
										<div class="cbp-l-caption-body">
											<div class="cbp-l-caption-text">KEBIDANAN</div>
										</div>
									</div>
								</div>
							</a>
							<a class="cbp-singlePage cbp-l-grid-team-name" style="font-size: 14px; text-decoration: none;cursor: pointer;">Bd. HEDDI PARDEDE, Amd. Keb</a>
							<div class="cbp-l-grid-team-position" style="font-size: 12px;">SIP: No.503/629/Dinkes/BD/2018</div>
						</li>

					</ul>
				</div>
			</div>
		</div>
	</div>

</section>
<!-- /Section: team -->

<!-- Section: works -->
<section id="facilities" class="home-section paddingbot-60">
	<div class="container marginbot-50">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<div class="wow fadeInDown" data-wow-delay="0.1s">
					<div class="section-heading text-center">
						<h2 class="h-bold">Fasilitas</h2>
						<p>Fasilitas dengan teknologi terbaru serta kenyamanan maksimal</p>
					</div>
				</div>
				<div class="divider-short"></div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12">
				<div class="wow bounceInUp" data-wow-delay="0.2s">
					<div id="owl-works" class="owl-carousel">

						<div class="item"><a href="<?php echo base_url() ?>assets/img/fasilitas/alat_dokter_gigi.jpg" title="Peralatan Medis Heddi Medica Dengan Teknologi Terbaru"
							 data-lightbox-gallery="gallery1" data-lightbox-hidpi="<?php echo base_url() ?>assets/img/fasilitas/alat_dokter_gigi.jpg"><img
								 src="<?php echo base_url() ?>assets/img/fasilitas/alat_dokter_gigi.jpg" class="img-responsive" alt="img"></a></div>

						<div class="item"><a href="<?php echo base_url() ?>assets/img/fasilitas/konsultasi.jpg" title="Konsultasi Dokter Gigi Heddi Medica"
							 data-lightbox-gallery="gallery1" data-lightbox-hidpi="<?php echo base_url() ?>assets/img/fasilitas/konsultasi.jpg"><img
								 src="<?php echo base_url() ?>assets/img/fasilitas/konsultasi.jpg" class="img-responsive" alt="img"></a></div>

						<div class="item"><a href="<?php echo base_url() ?>assets/img/fasilitas/pendaftaran.jpg" title="Pendaftaran Heddi Medica"
							 data-lightbox-gallery="gallery1" data-lightbox-hidpi="<?php echo base_url() ?>assets/img/fasilitas/pendaftaran.jpg"><img
								 src="<?php echo base_url() ?>assets/img/fasilitas/pendaftaran.jpg" class="img-responsive" alt="img"></a></div>

						<div class="item"><a href="<?php echo base_url() ?>assets/img/fasilitas/ruang_tunggu.jpg" title="Ruang Tunggu Pasien Heddi Medica"
							 data-lightbox-gallery="gallery1" data-lightbox-hidpi="<?php echo base_url() ?>assets/img/fasilitas/ruang_tunggu.jpg"><img
								 src="<?php echo base_url() ?>assets/img/fasilitas/ruang_tunggu.jpg" class="img-responsive" alt="img"></a></div>

						<div class="item"><a href="<?php echo base_url() ?>assets/img/fasilitas/parkir.jpg" title="Lahan Parkir Heddi Medica"
							 data-lightbox-gallery="gallery1" data-lightbox-hidpi="<?php echo base_url() ?>assets/img/fasilitas/parkir.jpg"><img
								 src="<?php echo base_url() ?>assets/img/fasilitas/parkir.jpg" class="img-responsive" alt="img"></a></div>

						<div class="item"><a href="<?php echo base_url() ?>assets/img/fasilitas/kamar_rawat.jpg" title="Kamar Rawat Inap & Bersalin Heddi Medica"
							 data-lightbox-gallery="gallery1" data-lightbox-hidpi="<?php echo base_url() ?>assets/img/fasilitas/kamar_rawat.jpg"><img
								 src="<?php echo base_url() ?>assets/img/fasilitas/kamar_rawat.jpg" class="img-responsive" alt="img"></a></div>

						<div class="item"><a href="<?php echo base_url() ?>assets/img/fasilitas/r_bersalin.jpg" title="Ruang Bersalin Heddi Medica"
							 data-lightbox-gallery="gallery1" data-lightbox-hidpi="<?php echo base_url() ?>assets/img/fasilitas/r_bersalin.jpg"><img
								 src="<?php echo base_url() ?>assets/img/fasilitas/r_bersalin.jpg" class="img-responsive" alt="img"></a></div>

						<div class="item"><a href="<?php echo base_url() ?>assets/img/fasilitas/apar.jpg" title="Tanggap Darurat Heddi Medica"
							 data-lightbox-gallery="gallery1" data-lightbox-hidpi="<?php echo base_url() ?>assets/img/fasilitas/apar.jpg"><img
								 src="<?php echo base_url() ?>assets/img/fasilitas/apar.jpg" class="img-responsive" alt="img"></a></div>

						<div class="item"><a href="<?php echo base_url() ?>assets/img/fasilitas/wastefall.jpg" title="Standar Kebersihan Heddi Medica"
							 data-lightbox-gallery="gallery1" data-lightbox-hidpi="<?php echo base_url() ?>assets/img/fasilitas/wastefall.jpg"><img
								 src="<?php echo base_url() ?>assets/img/fasilitas/wastefall.jpg" class="img-responsive" alt="img"></a></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /Section: works -->

<section id="promo" class="home-section paddingbot-60">
	<div class="container marginbot-50">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<div class="wow lightSpeedIn" data-wow-delay="0.1s">
					<div class="section-heading text-center">
						<h2 class="h-bold">Promo</h2>
						<p>Banyak penawaran menarik kami sediakan untuk anda</p>
					</div>
				</div>
				<div class="divider-short"></div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div style="padding: 10px;" class="col-sm-12 col-md-12">
				<div class="partner">
					<a href="<?php echo base_url() ?>assets/img/promo/promo2.jpg"><img style="width: 100%; max-width: 400px;" src="<?php echo base_url() ?>assets/img/promo/promo2.jpg" /></a>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Section: pricing -->
<section id="pricing" class="home-section bg-gray paddingbot-60">
	<div class="container marginbot-50">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<div class="wow lightSpeedIn" data-wow-delay="0.1s">
					<div class="section-heading text-center">
						<h2 class="h-bold">Jadwal Praktek</h2>
						<p></p>
					</div>
				</div>
				<div class="divider-short"></div>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row">

			<div class="col-sm-4 pricing-box">
				<div class="wow bounceInUp" data-wow-delay="0.1s">
					<div class="pricing-content general" style="height: 500px;">
						<h3>Dokter Umum</h3>
						<ul>
							<li style="font-weight: bold;">Dr. Siti <br>SIP: No.503/470/Dinkes/DU/2018</li>
							<li>Senin & Jum'at jam 15.00 - 18.30 <br>Selasa - Sabtu jam 08.00 - 11.00</li>
						</ul>
						<ul>
							<li style="font-weight: bold;">Dr. Fitriani Susanti <br>SIP: No.503/532/Dinkes/DU/2018</li>
							<li>Senin & Jum'at jam 08.00 - 11.00 <br>Selasa - Sabtu jam 15.30 - 18.30</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-sm-4 pricing-box featured-price">
				<div class="wow bounceInUp" data-wow-delay="0.3s">
					<div class="pricing-content featured" style="height: 500px;">
						<h3>Dokter Gigi</h3>
						<ul>
							<li style="font-weight: bold;">Dr. Winda Mawaddah <br>SIP: No.503/143/Dinkes/Drg/2018</li>
							<li>Senin, Selasa, Kamis jam 08.00 - 11.00 <br>Rabu, Jum'at, Sabtu jam 15.30 - 18.30</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-sm-4 pricing-box">
				<div class="wow bounceInUp" data-wow-delay="0.2s">
					<div class="pricing-content general last" style="height: 500px;">
						<h3>Bidan & Perawat</h3>
						<ul>
							<li style="font-weight: bold;">Heddi Pardede, AMD.Keb <br>SIP: No.503/629/Dinkes/BD/2018</li>
							<li style="font-weight: bold;">NS. Lisbet Pardede, S.Kep <br>SIP: No.503/662/Dinkes/Pr/2018</li>
							<li>Senin - Sabtu jam 08.00 - 20.00 <br>Minggu jam 13.00 - 18.30</li>
						</ul>
					</div>
				</div>
			</div>

		</div>

	</div>
</section>
<!-- /Section: pricing -->

<section id="partner" class="home-section paddingbot-60">
	<div class="container marginbot-50">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<div class="wow lightSpeedIn" data-wow-delay="0.1s">
					<div class="section-heading text-center">
						<h2 class="h-bold">Partner Kami</h2>
						<p>Bersama membangun masyarakat sehat dan produktif</p>
					</div>
				</div>
				<div class="divider-short"></div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-6">
				<div class="partner">
					<a href="https://bpjs-kesehatan.go.id/bpjs/"><img src="<?php echo base_url() ?>assets/img/bpjs.png" alt="" /></a>
				</div>
			</div>
			<div class="col-sm-6 col-md-6">
				<div class="partner">
					<a href="http://www.kotaindustri.com/"><img src="<?php echo base_url() ?>assets/img/kota.png" alt="" /></a>
				</div>
			</div>
		</div>
	</div>
</section>