<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Web_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
    }
    public function index()
    {
        $this->load->view('home');
    }

    public function home_form()
    {
        $data['specialists'] = $this->main_model->gda1p('specialists');
        $this->load->view('home_form', $data);
    }

    public function home_articles($page)
    {
        $perPage = 10;
        $total_row = $this->main_model->count('posts', 'id');
        $data['all'] = ceil($total_row / 10);
        if ($page != 1) {
            $end = $perPage * $page;
            $start = $end - 10;
            $data['page'] = $page;
            if ($start > 0) {
                $data['posts'] = $this->main_model->limit($start, $end, 'posts', 'status', 'active');
                $this->load->view('home_articles', $data);
            } else {
                $data['posts'] = '';
                $this->load->view('home_articles', $data);
            }
        } else {
            $data['page'] = $page;
            $data['posts'] = $this->main_model->limit(0, $perPage, 'posts', 'status', 'active');
            $this->load->view('home_articles', $data);
        }
    }

    public function store()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['name'] = $obj->name;
        $data['specialist'] = $obj->specialist;
        $data['email'] = $obj->email;
        $data['phone'] = $obj->phone;
        $data['message'] = $obj->message;
        $data['status'] = 'unread';
        $data['created_at'] = date('Y-m-d H:i:s');
        $store = $this->main_model->store('webmessage', $data);
        if ($store) {
            $status = array('status' => 'success');
            $this->mylib->setJSON();
            echo json_encode($status);
        }
    }

    public function read($slug)
    {
       $data['post'] = $this->main_model->gda5p('posts', 'slug', $slug, 'status', 'active');
       $data['recent'] = $this->main_model->limit(0, 5, 'posts', 'status', 'active');
       $data['specialists'] = $this->main_model->gda1p('specialists');
       $this->load->view('read', $data);
    }
}
