<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if (!function_exists('send_mail')) {

    function send_mail($email, $message, $subject)
    {
        $CI = &get_instance();
        $CI->load->model('main_model');
        $setting = $CI->main_model->gda1p('setting');
        //Load email library
        $CI->load->library('email');

        //SMTP & mail configuration
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => $setting[0]['send_mail'],
            'smtp_pass' => $setting[0]['send_pass'],
            'mailtype' => 'html',
            'charset' => 'utf-8',
        );
        $CI->email->initialize($config);
        $CI->email->set_mailtype("html");
        $CI->email->set_newline("\r\n");

        $CI->email->to($email);
        $CI->email->from($setting[0]['send_mail'], 'Klinik Heddi Medica');
        $CI->email->subject($subject);
        $CI->email->message($message);

        //Send email
        $status = $CI->email->send();

        return $status;
    }

}

if (!function_exists('torp')) {

    function torp($number)
    {
        return 'Rp. ' . strrev(implode('.', str_split(strrev(strval($number)), 3)));
    }

}

if ( ! function_exists( 'array_key_last' ) ) {
    /**
     * Polyfill for array_key_last() function added in PHP 7.3.
     *
     * Get the last key of the given array without affecting
     * the internal array pointer.
     *
     * @param array $array An array
     *
     * @return mixed The last key of array if the array is not empty; NULL otherwise.
     */
    function array_key_last( $array ) {
        $key = NULL;

        if ( is_array( $array ) ) {

            end( $array );
            $key = key( $array );
        }

        return $key;
    }
}

if (!function_exists('arraySearch')) {
    function arraySearch($array, $field, $search)
    {
        foreach ($array as $key => $value) {
            if ($value[$field] === $search) {
                return $key;
            }
        }
        return false;
    }
}

if (!function_exists('random_string')) {
    function random_string($length)
    {
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= $chars[rand(0, strlen($chars) - 1)];

        }
        return $string;
    }
}

if (!function_exists('to_date_time')) {
    function to_date_time($date)
    {

        $bersih = explode(' ', $date);

        $pisah = explode('-', $bersih[0]);

        if ($pisah[1] == '01') {
            $pisah[1] = 'Januari';
        } else if ($pisah[1] == '02') {
            $pisah[1] = 'Februari';
        } else if ($pisah[1] == '03') {
            $pisah[1] = 'Maret';
        } else if ($pisah[1] == '04') {
            $pisah[1] = 'April';
        } else if ($pisah[1] == '05') {
            $pisah[1] = 'Mei';
        } else if ($pisah[1] == '06') {
            $pisah[1] = 'Juni';
        } else if ($pisah[1] == '07') {
            $pisah[1] = 'Juli';
        } else if ($pisah[1] == '08') {
            $pisah[1] = 'Agustus';
        } else if ($pisah[1] == '09') {
            $pisah[1] = 'September';
        } else if ($pisah[1] == '10') {
            $pisah[1] = 'Oktober';
        } else if ($pisah[1] == '11') {
            $pisah[1] = 'November';
        } else if ($pisah[1] == '12') {
            $pisah[1] = 'Desember';
        }

        $urutan = array($pisah[2], $pisah[1], $pisah[0]);
        $satukan = implode(' ', $urutan);

        return $satukan . ' ' . $bersih[1];

    }
}

if (!function_exists('to_date')) {
    function to_date($date)
    {

        $pisah = explode('-', $date);

        if ($pisah[1] == '01') {
            $pisah[1] = 'Januari';
        } else if ($pisah[1] == '02') {
            $pisah[1] = 'Februari';
        } else if ($pisah[1] == '03') {
            $pisah[1] = 'Maret';
        } else if ($pisah[1] == '04') {
            $pisah[1] = 'April';
        } else if ($pisah[1] == '05') {
            $pisah[1] = 'Mei';
        } else if ($pisah[1] == '06') {
            $pisah[1] = 'Juni';
        } else if ($pisah[1] == '07') {
            $pisah[1] = 'Juli';
        } else if ($pisah[1] == '08') {
            $pisah[1] = 'Agustus';
        } else if ($pisah[1] == '09') {
            $pisah[1] = 'September';
        } else if ($pisah[1] == '10') {
            $pisah[1] = 'Oktober';
        } else if ($pisah[1] == '11') {
            $pisah[1] = 'November';
        } else if ($pisah[1] == '12') {
            $pisah[1] = 'Desember';
        }

        $urutan = array($pisah[2], $pisah[1], $pisah[0]);
        $satukan = implode(' ', $urutan);

        return $satukan;

    }
}

if (!function_exists('to_month')) {
    function to_month($month)
    {

        if ($month == '01') {
            $month = 'Januari';
        } else if ($month == '02') {
            $month = 'Februari';
        } else if ($month == '03') {
            $month = 'Maret';
        } else if ($month == '04') {
            $month = 'April';
        } else if ($month == '05') {
            $month = 'Mei';
        } else if ($month == '06') {
            $month = 'Juni';
        } else if ($month == '07') {
            $month = 'Juli';
        } else if ($month == '08') {
            $month = 'Agustus';
        } else if ($month == '09') {
            $month = 'September';
        } else if ($month == '10') {
            $month = 'Oktober';
        } else if ($month == '11') {
            $month = 'November';
        } else if ($month == '12') {
            $month = 'Desember';
        }

        return $month;

    }
}

if (!function_exists('limiter')) {
    function limiter($string, $l)
    {
        $string = strip_tags($string);
        if (strlen($string) > $l) {

            // truncate string
            $stringCut = substr($string, 0, $l);
            $endPoint = strrpos($stringCut, ' ');

            //if the string doesn't contain any space then it will cut without word basis.
            $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
            $string .= ' ...';
        }
        echo $string;
    }
}

if (!function_exists('to_dump')) {
    function to_dump($data)
    {
        echo '<pre>';
        var_dump($data);
        die();
        echo '</pre>';
    }
}
