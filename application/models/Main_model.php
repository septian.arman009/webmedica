<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main_model extends CI_Model
{
    public function store($table, $data)
    {
        return $this->db->insert($table, $data);
    }

    public function gda1p($table)
    {
        return $this->db->get($table)->result_array();
    }

    public function gdo4p($table, $take, $column, $id)
    {
        $this->db->where($column, $id);
        $query = $this->db->get($table)->result_array();
        if ($query) {
            if ($query[0][$take]) {
                return $query[0][$take];
            } else {
                return 0;
            }
        }
    }

    public function gda3p($table, $column, $id)
    {
        $this->db->where($column, $id);
        return $this->db->get($table)->result_array();
    }

    public function gda5p($table, $column, $id, $column1, $id1)
    {
        $this->db->where($column, $id);
        $this->db->where($column1, $id1);
        return $this->db->get($table)->result_array();
    }

    public function count($table, $id)
    {
        $query = $this->db->query("select count($id) as total_row from $table")->result_array();
        if ($query) {
            if ($query[0]['total_row'] != '') {
                return $query[0]['total_row'];
            } else {
                return 0;
            }
        }
    }

    public function limit($start, $content_per_page, $table, $column, $key)
    {
        $sql = "select * from  $table where $column = '$key' order by id desc LIMIT $start,$content_per_page";
        $result = $this->db->query($sql);
        return $result->result_array(); 
    }

}
