 
function postData(url, data, callback) {
	$.ajax({
		type: "POST",
		contentType: "application/json",
		dataType: "json",
		url: url,
		data: JSON.stringify(data),
		success: function (response) {
			return callback(null, response);
		},
		error: function (err) {
			return callback(true, err);
		}
	});
}

function ValidateEmail(mail, btn, id_msg) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
		$(id_msg).hide();
		return true;
	}
	$(id_msg).show();
	$(btn).attr("disabled", "disabled");
}
